defmodule Start do
  use Application

  alias ConnectFour.Bot

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = Application.get_env(:exirc_example, :bots)
               |> Enum.map(fn bot -> worker(ConnectFour, [bot]) end)

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Start.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
